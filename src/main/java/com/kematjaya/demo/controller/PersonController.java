
package com.kematjaya.demo.controller;

import com.kematjaya.demo.entity.Person;
import com.kematjaya.demo.repository.PersonRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Controller
@RequestMapping(path = "/person")
public class PersonController 
{
    @Autowired
    private PersonRepository personRepository;
    
    @GetMapping(path="/list")
    public String index(Model model)
    {
        model.addAttribute("persons", personRepository.findAll());
        
        return "person/index";
    }
    
    @GetMapping(path="/create")
    public String create(Person person)
    {
        return "person/create";
    }
    
    @PostMapping(path = "/insert")
    public RedirectView insert(@Valid Person person, BindingResult result, Model model)
    {
        if (result.hasErrors()) {
            return new RedirectView("/person/create");
        }
        
        personRepository.save(person);
        
        return new RedirectView("/person/list");
    }
    
    @GetMapping(path="/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model)
    {
        Person person = personRepository.findById(id)
            .orElseThrow(
                ()-> new IllegalArgumentException("Invalid student Id:" + id)
            );
        
        model.addAttribute("person", person);
        
        return "person/edit";
    }
    
    @PostMapping(path="/update/{id}")
    public RedirectView update(@PathVariable("id") int id, @Valid Person person, BindingResult result, Model model, RedirectAttributes redirectAttributes) 
    {
        if (result.hasErrors()) {
            person.setId(id);
            redirectAttributes.addAttribute("id", id);
            
            return new RedirectView("/person/edit/{id}");
        }
        
        personRepository.save(person);
        
        return new RedirectView("/person/list");
    }
    
    @GetMapping(path = "/delete/{id}")
    public RedirectView remove(@PathVariable("id") int id)
    {
        Person person = personRepository.findById(id)
            .orElseThrow(
                ()-> new IllegalArgumentException("Invalid student Id:" + id)
            );
        
        personRepository.delete(person);
        
        return new RedirectView("/person/list");
    }
    
    @GetMapping(path="/api")
    public @ResponseBody Iterable<Person> getAllUsers() 
    {
        return personRepository.findAll();
    }
}
