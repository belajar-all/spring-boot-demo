
package com.kematjaya.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.validation.constraints.NotBlank;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @NotBlank(message="Name is mandatory")
    @Column(name="name")
    private String name;

    @Column(name="email")
    private String email;
    
    @Column(name="phone_no")
    private String phoneNo;

    public Integer getId() 
    {
        return id;
    }

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getPhoneNo() 
    {
        return phoneNo;
    }
    
    public void setPhoneNo(String phoneNo) 
    {
        this.phoneNo = phoneNo;
    }
    
    
}
