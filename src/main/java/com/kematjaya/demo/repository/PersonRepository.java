
package com.kematjaya.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.kematjaya.demo.entity.Person;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
public interface PersonRepository extends CrudRepository<Person, Integer> 
{

}
